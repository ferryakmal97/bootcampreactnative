var nomor = 2;
console.log('LOOPING PERTAMA');
while(nomor <= 20) {
  console.log(nomor, '- I Love Coding');
  nomor=nomor+2;
}

var nomor = 20;
console.log('LOOPING KEDUA');
while(nomor >= 2) {
  console.log(nomor, '- I will become a mobile developer');
  nomor=nomor-2;
}

console.log('Looping menggunakan for');
for(i=1;i<=20;i++){
    if (i%3===0 || i===1){
        console.log(i,' - Santai');
    } else if (i%2===0){
        console.log(i,' - Berkualitas');
    } else if (i%2!=0 && i%3!=0){
        console.log(i,' - I Love Coding')
    }
}

var x;
console.log('Membuat Persegi Panjang')
for(x=0; x<=3;x++){
    console.log('########');
}

var x,y;
var p='';
console.log('Membuat Tangga')
for(x=0; x<=6;x++){
    for(y=0;y<=x;y++){
        p += '#'
    }console.log(p);
    p = '';
}

console.log('Membuat Papan Catur');
var p='';
for(i=1;i<=8;i++){
   if(i%2!==0) {
    for(y=0;y<=7;y++){
        if (y%2==0){
            p += ' '
        } else {
            p += '#'
        }
    }console.log(p);
    p = '';
   } else {
    for(y=0;y<=7;y++){
        if (y%2==0){
            p += '#'
        } else {
            p += ' '
        }
    }console.log(p);
    p = '';
   }
}