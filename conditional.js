//TUGAS CONDITIONAL : IF-ELSE
var nama = "JEKI"
var peran = ['penyihir', 'guard', 'werewolf']
var peranPemain = 'werewolf'

if ( nama == false) {
    console.log("Nama harus diisi!")
} else if (peranPemain == false) {
    console.log("Halo " +nama, "pilih peranmu untuk memulai game!")
} else if (peranPemain == peran[0]) {
    console.log("Selamat datang di Dunia Werewolf, " +nama)
    console.log("Halo Penyihir " +nama,", kamu dapat melihat siapa yang menjadi werewolf!")
} else if (peranPemain == peran[1]) {
    console.log("Selamat datang di Dunia Werewolf, " +nama)
    console.log("Halo Guard " +nama,", kamu akan membantu melindungi temanmu dari serangan werewolf!")
} else if (peranPemain == peran[2]) {
    console.log("Selamat datang di Dunia Werewolf, " +nama)
    console.log("Halo Werewolf " +nama,", Kamu akan memakan mangsa setiap malam!")
} else {
    console.log("Halo " +nama,", peran kamu tidak terdaftar dalam game ini, silahkan cari peran lain")
}


//TUGAS CONDITIONAL : SWITCH CASE
var tanggal = 21; 
var bulan = 1; 
var tahun = 1945;

if (tanggal >=1 && tanggal <=31) {
    switch(tanggal) {
        case tanggal: { console.log(+tanggal);break;}
    }
} else if (tanggal != Number){
    switch(tanggal) {
        default:  { console.log('Tanggal Salah !'); }
    }
} else {
    switch(tanggal) {
        default:  { console.log('Tanggal Belum Diisi !'); }
    }
}

switch(bulan) {
    case 1:   { console.log('Januari'); break; }
    case 2:   { console.log('Februari'); break; }
    case 3:   { console.log('Maret'); break; }
    case 4:   { console.log('April'); break; }
    case 5:  { console.log('Mei'); }
    case 6:   { console.log('Juni'); break; }
    case 7:   { console.log('Juli'); break; }
    case 8:   { console.log('Agustus'); break; }
    case 9:   { console.log('September'); break; }
    case 10:  { console.log('Oktober'); }
    case 11:   { console.log('November'); break; }
    case 12:  { console.log('Desember'); }
    default:  { console.log('PILIH BULAN !'); }
}

if (tahun >=1900 && tanggal <=2200) {
    switch(tahun) {
        case tahun: { console.log(+tahun);break;}
    }
} else if (tahun != Number){
    switch(tahun) {
        default:  { console.log('tahun Salah !'); }
    }
} else {
    switch(tahun) {
        default:  { console.log('tahun Belum Diisi !'); }
    }
}